// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
const { desktopCapturer } = require("electron");

pc = new RTCPeerConnection({});

desktopCapturer.getSources(
  { types: ["window", "screen"] },
  (error, sources) => {
    if (error) throw error;
    if (sources.length === 0) throw "Not Enough Sources";
    var theSource = sources[0].id;
    navigator.mediaDevices
      .getUserMedia({
        audio: false,
        video: {
          mandatory: {
            chromeMediaSource: "desktop",
            chromeMediaSourceId: theSource,
            maxWidth: 1280,
            maxHeight: 1024,
            maxFrameRate: 15
          }
        }
      })
      .then(stream => handleStream(stream))
      .catch(e => handleError(e));

    return;
  }
);

function handleStream(stream) {
  pc.addStream(stream);
  pc
    .createOffer()
    .then(offer => {
      // preliminary offer
      pc.setLocalDescription(offer);
    })
    .catch(reason => {
      console.log("CreateOffer failed: " + reason);
    });
  pc.onicecandidate = event => {
    var state = pc.iceGatheringState;
    if (state === "complete") {
      pc
        .createOffer()
        .then(offer => {
          console.log("CreateOffer succeeded: " + offer.sdp);
          // final offer
          document.querySelector("#mySdpOffer").value = offer.sdp;
        })
        .catch(reason => {
          console.log("CreateOffer failed: " + reason);
        });
    }
  };
  const video = document.querySelector("#localVideo");
  video.srcObject = stream;
  video.onloadedmetadata = e => video.play();
}

const button = document.querySelector("button");
const mySdp = document.querySelector("#mySdpOffer");
const remoteSdp = document.querySelector("#remoteSdpAnswer");

button.onclick = () => {
  pc.setRemoteDescription({ type: "answer", sdp: remoteSdp.value });
};

pc.onaddstream = function(evt) {
  const video = document.querySelector("#remoteVideo");
  video.srcObject = evt.stream;
  video.onloadedmetadata = e => video.play();
};

function handleError(e) {
  console.log(e);
}
